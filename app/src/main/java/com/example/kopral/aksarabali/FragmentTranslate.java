package com.example.kopral.aksarabali;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.kopral.CONST.AKSARA;
import com.example.kopral.CONST.CONFIG;
import com.example.kopral.konverter.CekJenisHuruf;
import com.example.kopral.konverter.KonversiHrf_A;
import com.example.kopral.konverter.KonversiHrf_B;
import com.example.kopral.konverter.KonversiHrf_C;
import com.example.kopral.konverter.KonversiHrf_D;
import com.example.kopral.konverter.KonversiHrf_E;
import com.example.kopral.konverter.KonversiHrf_Ee;
import com.example.kopral.konverter.KonversiHrf_G;
import com.example.kopral.konverter.KonversiHrf_H;
import com.example.kopral.konverter.KonversiHrf_I;
import com.example.kopral.konverter.KonversiHrf_J;
import com.example.kopral.konverter.KonversiHrf_K;
import com.example.kopral.konverter.KonversiHrf_L;
import com.example.kopral.konverter.KonversiHrf_M;
import com.example.kopral.konverter.KonversiHrf_N;
import com.example.kopral.konverter.KonversiHrf_O;
import com.example.kopral.konverter.KonversiHrf_P;
import com.example.kopral.konverter.KonversiHrf_R;
import com.example.kopral.konverter.KonversiHrf_S;
import com.example.kopral.konverter.KonversiHrf_T;
import com.example.kopral.konverter.KonversiHrf_U;
import com.example.kopral.konverter.KonversiHrf_W;
import com.example.kopral.konverter.KonversiHrf_Y;
import com.example.kopral.konverter.OlahKata;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static android.R.id.list;

/**
 * Created by Kopral on 18/09/16.
 */


public class FragmentTranslate extends Fragment {

    DatabaseHelper db;

    EditText et_latin_content;
    TextView tv_aksara_content;
    TextView tv_aksara_content_before;
    WebView wv;
    LinearLayout linLay_blank;
    CardView card_aksara;
    Button btn_aksara_action_convert;
    RelativeLayout relLay_aksara_text_container;
    AndroidBmpUtil abu;
    //=====new script======
    private String aksaraBali;
    private boolean cecek;
    public String fontPath;
    private boolean gantungan;
    private String hurufLatin;
    int indexHrf;
    int indexHrfKonversi;
    int indexKata;
    private int jmlGantungan;
    boolean kataDiDB;
    private boolean kndsNG;
    KonversiHrf_A knvA;
    KonversiHrf_B knvB;
    KonversiHrf_C knvC;
    KonversiHrf_D knvD;
    KonversiHrf_E knvE;
    KonversiHrf_Ee knvEe;
    KonversiHrf_G knvG;
    KonversiHrf_H knvH;
    KonversiHrf_I knvI;
    KonversiHrf_J knvJ;
    KonversiHrf_K knvK;
    KonversiHrf_L knvL;
    KonversiHrf_M knvM;
    KonversiHrf_N knvN;
    KonversiHrf_O knvO;
    KonversiHrf_P knvP;
    KonversiHrf_R knvR;
    KonversiHrf_S knvS;
    KonversiHrf_T knvT;
    KonversiHrf_U knvU;
    KonversiHrf_W knvW;
    KonversiHrf_Y knvY;

    private String[] tempHasilKonversi;
    char[] tempKalimat;
    String[] tempKata;
    public Typeface tf;
    public TextView tvhasil;
    //
    CekJenisHuruf cjh;
    DBHelper helper;
    OlahKata ok;

    String str_aksara_tampil;
    String str_lastword_aksara;
    String str_lastword_latin;
    List<String> list_words_translated;
    List<String> list_words_latin;
    List<Integer> list_to_clear;

    ProgressDialog pDialog;
    RequestQueue requestQueue;
    Bitmap bm;
    File myFile;

    //settings
    SharedPreferences mySharedPreferences;
    String server_address;
    SharedPreferences.Editor editor;


    //=====================
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments

        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_translate, container, false);

        //new script
        this.gantungan = false;
        this.cecek = false;
        this.kndsNG = false;
        this.indexHrf = 0;
        this.indexKata = 0;
        this.indexHrfKonversi = 0;
        this.jmlGantungan = 0;
        this.cjh = new CekJenisHuruf();
        this.ok = new OlahKata("");
        this.kataDiDB = false;
        //==========

        abu = new AndroidBmpUtil();
        db = new DatabaseHelper(getActivity());

        list_words_translated = new ArrayList<String>();
        list_words_latin = new ArrayList<String>();
        list_to_clear = new ArrayList<Integer>();
        AKSARA.arlist_Translated = new ArrayList<HashMap<String, String>>();

        et_latin_content = (EditText) v.findViewById(R.id.et_latin_content);
        tv_aksara_content = (TextView) v.findViewById(R.id.tv_aksara_content);
        linLay_blank = (LinearLayout) v.findViewById(R.id.linLay_blank);
        card_aksara = (CardView) v.findViewById(R.id.card_aksara);
        btn_aksara_action_convert = (Button) v.findViewById(R.id.btn_aksara_action_convert);
        relLay_aksara_text_container = (RelativeLayout) v.findViewById(R.id.relLay_aksara_text_container);

        tv_aksara_content_before = (TextView) v.findViewById(R.id.tv_aksara_content_before);

        pDialog = new ProgressDialog(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());

        Typeface b_simbar = Typeface.createFromAsset(getActivity().getAssets(), "fonts/BaliSimbar-B.ttf");
        tv_aksara_content.setTypeface(b_simbar);

        mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        server_address = mySharedPreferences.getString("server_address", "192.168.43.221");

        btn_aksara_action_convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_aksara_content.setDrawingCacheEnabled(true);
                tv_aksara_content.buildDrawingCache(true);
                bm = tv_aksara_content.getDrawingCache(true);

                String fileName = "aksara_" + UUID.randomUUID().toString() + ".bmp";

                String sdcardBmpPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName;

                myFile = new File(sdcardBmpPath);
                if (myFile.exists()) {
                    if (myFile.delete()) {
                        Log.d("Aksara File", "DELETE: TRUE");
                        sendBitmap(sdcardBmpPath, bm);
                    }
                } else {
                    Log.d("Aksara File", "FILE NOT EXIST, create");
                    sendBitmap(sdcardBmpPath, bm);
                }

            }
        });

        et_latin_content.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //hapus();

                //String tekslatin = et_latin_content.getText().toString();
                //list_words_latin = new ArrayList<String>(Arrays.asList(tekslatin.split(" ")));
                String[] temp_latin = et_latin_content.getText().toString().split(" ");

                list_words_translated = new ArrayList<String>();
                //loop per kata untuk mentranslate perkata

                if (prosesTranslate(temp_latin)) { //translate
                    Log.d("getAksara", "arlist_Translateds: " + AKSARA.arlist_Translated.toString());
                    String str_aksara_tampil_from_arlist = "";
                    for (HashMap<String, String> mapItem : AKSARA.arlist_Translated) {
                        //listString += s + "\t";
                        str_aksara_tampil_from_arlist += mapItem.get("aksara") + " ";
                    }

                    tv_aksara_content.setText(str_aksara_tampil_from_arlist);
                    tv_aksara_content_before.setText(str_aksara_tampil_from_arlist);


                    /* str_aksara_tampil = TextUtils.join(" ", list_words_translated);
                    tv_aksara_content.setText(str_aksara_tampil);
                    tv_aksara_content_before.setText(str_aksara_tampil);*/
                }

                //str_lastword_latin = (temp_latin.length == 0) ?"":temp_latin[temp_latin.length-1];
               /* if (tekslatin.length() != 0) {
                    setHurufLatin(tekslatin);
                    //Log.d("HASIL", prosesKonversi());
                    String aks = prosesKonversi();
                    tv_aksara_content.setText(aks);
                    tv_aksara_content_before.setText(aks);

                }

                String firstChar = "";
                if (!tekslatin.isEmpty()) {
                    firstChar = tekslatin.substring(0, 1);
                }
                if (firstChar.equalsIgnoreCase("a") ||
                        firstChar.equalsIgnoreCase("i") ||
                        firstChar.equalsIgnoreCase("u") ||
                        firstChar.equalsIgnoreCase("e") ||
                        firstChar.equalsIgnoreCase("è") ||
                        firstChar.equalsIgnoreCase("é") ||
                        firstChar.equalsIgnoreCase("ê") ||
                        firstChar.equalsIgnoreCase("o")) {
                    String changefirst = "h" + firstChar;
                    tekslatin = changefirst + tekslatin.substring(1);
                }*/


                if (!et_latin_content.getText().toString().equalsIgnoreCase("")) {
                    linLay_blank.setVisibility(View.GONE);
                    card_aksara.setVisibility(View.VISIBLE);
                } else {
                    linLay_blank.setVisibility(View.VISIBLE);
                    card_aksara.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        String url = "file:///android_asset/www/index.html";
        wv = (WebView) v.findViewById(R.id.wv_aksara);
        wv.loadUrl(url);
        wv.addJavascriptInterface(new Jsnterface(getActivity()), "AndroidApp");
        WebSettings setup = wv.getSettings();
        setup.setJavaScriptEnabled(true);
        wv.loadUrl("javascript:getAksaraBali('saya suka buang sampah')");

        return v;
    }

    public boolean prosesTranslate(String[] val_temp_latin) {
        AKSARA.arlist_Translated = new ArrayList<HashMap<String, String>>();
        AKSARA.map_translated = new HashMap<String, String>();
        list_to_clear = new ArrayList<Integer>();
        //buat inisialisasi size
        for (int l = 0; l < val_temp_latin.length; l++) {
            AKSARA.arlist_Translated.add(AKSARA.map_translated);
        }
        Log.d("getAksara", "Size: " + AKSARA.arlist_Translated.size());

        int j = 0;
        for (j = 0; j < val_temp_latin.length; j++) {
            String tekslatin = val_temp_latin[j];
            ArrayList<HashMap<String, String>> arlist_getaksaralebihdarisatukata_fromdb = db.getAksaraLebihDariSatuKata(tekslatin);

            //hasil dari db lebih dari satu kata == 1
            if (arlist_getaksaralebihdarisatukata_fromdb.size() == 1) {
                // List<String> kataLebihDariSatu = new ArrayList<String>();

                String strKataDB = arlist_getaksaralebihdarisatukata_fromdb.get(0).get("latin");
                int jmlKataDB = strKataDB.split(" ").length;
                int jmlKataInput = val_temp_latin.length;
                int kataInputFrom = (j) - (jmlKataDB - 1);
                int kataInputTo = j + 1;

                if (jmlKataDB <= jmlKataInput && kataInputFrom >= 0) {
                    Log.d("getAksara", "j: " + j + " |jmlKataDB: " + jmlKataDB + " | kataInputFrom: " + String.valueOf(kataInputFrom) + " | kataInputTo: " + String.valueOf(kataInputTo) + " | val_temp_latin[j]: " + val_temp_latin[j] + " | strKataDB: " + strKataDB + " | val_temp_latin: " + TextUtils.join(" ", Arrays.asList(val_temp_latin)));
                    Log.d("getAksara", "hasil dari db lebih dari satu kata == 1: " + arlist_getaksaralebihdarisatukata_fromdb.toString() + " | val_temp_latin: " + Arrays.asList(val_temp_latin));

                    List<String> arrKataInput = Arrays.asList(val_temp_latin);
                    List<String> arrKataInputWillCompare = Arrays.asList(Arrays.copyOfRange(val_temp_latin, kataInputFrom, kataInputTo));
                    String strKataInput = TextUtils.join(" ", arrKataInputWillCompare);
                    Log.d("getAksara", "j: " + j + " |jmlKataDB: " + jmlKataDB + " | kataInputFrom: " + String.valueOf(kataInputFrom) + " | arrKataInput: " + arrKataInput.toString() + " | arrKataInputWillCompare: " + arrKataInputWillCompare.toString() + " | strKataInput: " + strKataInput + " | strKataDB: " + strKataDB);

                    //DB sesuai dengan input
                    if (strKataDB.equalsIgnoreCase(strKataInput)) {
                        for (int m = kataInputFrom; m < kataInputTo - 1; m++) {
                            //Log.d("getAksara","clearing - "+" m: "+m+" | kataInpuTo: "+kataInputTo);
                            //kosongin
                            list_to_clear.add(m);
                        }

                        clear_that_exists_in_db();
                        Log.d("getAksara", "j: " + j + " |list_to_clear: " + list_to_clear.toString());

                        AKSARA.map_translated = new HashMap<String, String>();
                        AKSARA.map_translated.put("latin", arlist_getaksaralebihdarisatukata_fromdb.get(0).get("latin"));
                        AKSARA.map_translated.put("aksara", arlist_getaksaralebihdarisatukata_fromdb.get(0).get("aksara"));
                        AKSARA.arlist_Translated.set(j, AKSARA.map_translated);
                        Log.d("getAksara", "hasil clearing - " + AKSARA.arlist_Translated.toString());


                    } else { //ternyata gak sesuai bro
                        eksekusiAksaraBiasa(j, tekslatin);
                    }
                }else{
                    eksekusiAksaraBiasa(j, tekslatin);
                }
            } else if (arlist_getaksaralebihdarisatukata_fromdb.size() > 1) { //ditemukan di db lebih dari satu wildcard
                list_to_clear = new ArrayList<Integer>();

                Log.d("getAksara", "hasil dari db lebih dari satu kata > 1");
                for (int n = 0; n < arlist_getaksaralebihdarisatukata_fromdb.size(); n++) {
                    String strKataDB = arlist_getaksaralebihdarisatukata_fromdb.get(n).get("latin");
                    int jmlKataDB = strKataDB.split(" ").length;
                    int jmlKataInput = val_temp_latin.length;
                    int kataInputFrom = (j) - (jmlKataDB - 1);
                    int kataInputTo = j + 1;

                    if (jmlKataDB <= jmlKataInput && kataInputFrom >= 0) {
                        Log.d("getAksara", "j: " + j + " |jmlKataDB: " + jmlKataDB + " | kataInputFrom: " + String.valueOf(kataInputFrom) + " | kataInputTo: " + String.valueOf(kataInputTo) + " | val_temp_latin[j]: " + val_temp_latin[j] + " | strKataDB: " + strKataDB + " | val_temp_latin: " + TextUtils.join(" ", Arrays.asList(val_temp_latin)));
                        Log.d("getAksara", "hasil dari db lebih dari satu kata == 1: " + arlist_getaksaralebihdarisatukata_fromdb.toString() + " | val_temp_latin: " + Arrays.asList(val_temp_latin));

                        List<String> arrKataInput = Arrays.asList(val_temp_latin);
                        List<String> arrKataInputWillCompare = Arrays.asList(Arrays.copyOfRange(val_temp_latin, kataInputFrom, kataInputTo));
                        String strKataInput = TextUtils.join(" ", arrKataInputWillCompare);
                        Log.d("getAksara", "j: " + j + " |jmlKataDB: " + jmlKataDB + " | kataInputFrom: " + String.valueOf(kataInputFrom) + " | arrKataInput: " + arrKataInput.toString() + " | arrKataInputWillCompare: " + arrKataInputWillCompare.toString() + " | strKataInput: " + strKataInput + " | strKataDB: " + strKataDB);

                        //DB sesuai dengan input
                        if (strKataDB.equalsIgnoreCase(strKataInput)) {
                            for (int m = kataInputFrom; m < kataInputTo - 1; m++) {
                                //Log.d("getAksara","clearing - "+" m: "+m+" | kataInpuTo: "+kataInputTo);
                                //kosongin
                                list_to_clear.add(m);
                            }

                            clear_that_exists_in_db();
                            Log.d("getAksara", "j: " + j + " |list_to_clear: " + list_to_clear.toString());

                            AKSARA.map_translated = new HashMap<String, String>();
                            AKSARA.map_translated.put("latin", arlist_getaksaralebihdarisatukata_fromdb.get(0).get("latin"));
                            AKSARA.map_translated.put("aksara", arlist_getaksaralebihdarisatukata_fromdb.get(0).get("aksara"));
                            AKSARA.arlist_Translated.set(j, AKSARA.map_translated);
                            Log.d("getAksara", "hasil clearing - " + AKSARA.arlist_Translated.toString());


                        } else { //ternyata gak sesuai bro
                            eksekusiAksaraBiasa(j, tekslatin);
                        }
                    } else {
                        eksekusiAksaraBiasa(j, tekslatin);
                    }
                }
            } else {
                eksekusiAksaraBiasa(j, tekslatin);
            }


        }
        return true;
    }

    private void eksekusiAksaraBiasa(int j, String tekslatin) {
        ArrayList<HashMap<String, String>> arlist_getaksara_fromdb = db.getAksara(tekslatin);
        Log.d("getAksara", j + ". Eksekusi biasa | Teks Latin: " + tekslatin);

        /*Log.d("getAksara", "teksLatin: "+tekslatin);
        AKSARA.map_translated = new HashMap<String,String>();
        AKSARA.map_translated.put("latin",tekslatin);
        AKSARA.arlist_Translated.add(AKSARA.map_translated);
        Log.d("getAksara","arlist_Translated: "+ AKSARA.arlist_Translated);*/
        Log.d("getAksara", j + " | arlist_getaksara_fromdb.size(): " + arlist_getaksara_fromdb.size());

        if (arlist_getaksara_fromdb.size() != 0) {
            //get from db

            //AKSARA.map_translated = new HashMap<String,String>();
            //AKSARA.map_translated.put("latin",arlist_getaksara_fromdb.get(0).get("latin"));
            //AKSARA.map_translated.put("aksara",arlist_getaksara_fromdb.get(0).get("aksara"));
            //AKSARA.arlist_Translated.add(AKSARA.map_translated);

            list_words_translated.add(arlist_getaksara_fromdb.get(0).get("aksara"));
            AKSARA.map_translated = new HashMap<String, String>();
            AKSARA.map_translated.put("latin", arlist_getaksara_fromdb.get(0).get("latin"));
            AKSARA.map_translated.put("aksara", arlist_getaksara_fromdb.get(0).get("aksara"));
            AKSARA.arlist_Translated.set(j, AKSARA.map_translated);

            //str_aksara_tampil = TextUtils.join(" ", list_words_translated);
            //Log.d("getAksara", "DB - kata: " + list_words_translated.toString());
            //Log.d("getAksara","teksLatinDB: "+tekslatin);
            Log.d("getAksara", j + ". arlist_Translated: " + AKSARA.arlist_Translated + " | arlist_getaksara_fromdb.size(): " + arlist_getaksara_fromdb.size());
        } else {
            //translate using algorithm
            tekslatin = tekslatin.replace(" a", " ha");
            tekslatin = tekslatin.replace(" i", " hi");
            tekslatin = tekslatin.replace(" u", " hu");
            tekslatin = tekslatin.replace(" e", " he");
            tekslatin = tekslatin.replace(" è", " hè");
            tekslatin = tekslatin.replace(" é", " hé");
            tekslatin = tekslatin.replace(" ê", " hê");
            tekslatin = tekslatin.replace(" o", " ho");

            //str_lastword_latin = tekslatin.replaceAll("(\\r|\\n|\\r\\n)+", "\\\\n");

            // AKSARA.map_translated.put("aksara","");
            // AKSARA.arlist_Translated.add(AKSARA.map_translated);
            //Log.d("getAksara","teksLatinAlgo: "+tekslatin);

            wv.loadUrl("javascript:getAksaraBali('" + j + "','" + tekslatin + "');");
            //Log.d("getAksara", "Bukan DB - kata: " + tekslatin);
        }
    }

    public boolean clear_that_exists_in_db() {
        for (int o = 0; o < list_to_clear.size(); o++) {
            AKSARA.map_translated = new HashMap<String, String>();
            AKSARA.map_translated.put("latin", "");
            AKSARA.map_translated.put("aksara", "");
            AKSARA.arlist_Translated.set(list_to_clear.get(o), AKSARA.map_translated);
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
        // return;
        // return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("MENU item", "ID: " + item.getItemId());
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                alert.setTitle("Alamat Server");
                // alert.setMessage("Masukkan Alamat Server");

                // Set an EditText view to get user input
                final EditText input = new EditText(getActivity());
                input.setText(server_address);

                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        editor = mySharedPreferences.edit();
                        editor.putString("server_address", input.getText().toString());

                        if (editor.commit()) {
                            server_address = mySharedPreferences.getString("server_address", "192.168.43.221");
                        }
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });
                alert.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Translate");
    }

    public void translateTroughJS(String valTeksLatin) {

    }

    public class Jsnterface {

        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        Jsnterface(Context c) {
            mContext = c;
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void getAksara(final String j, final String latin, final String translated) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Log.d("getaksara", "val from js: " + val);
                    //list_words_translated.add(val);
                    //str_aksara_tampil = TextUtils.join(" ", list_words_translated);

                    //Log.d("getAksara", "arlist_Translated algo: " + AKSARA.arlist_Translated);

                    try {
                        AKSARA.map_translated = new HashMap<String, String>();
                        AKSARA.map_translated.put("latin", latin);
                        AKSARA.map_translated.put("aksara", translated);
                        AKSARA.arlist_Translated.set(Integer.parseInt(j), AKSARA.map_translated);

                        clear_that_exists_in_db();

                        Log.d("getAksara", j + ". arlist_Translated algo: " + AKSARA.arlist_Translated);

                        String str_aksara_tampil_from_arlist = "";
                        for (HashMap<String, String> mapItem : AKSARA.arlist_Translated) {
                            //listString += s + "\t";
                            str_aksara_tampil_from_arlist += mapItem.get("aksara") + " ";
                        }

                        tv_aksara_content.setText(str_aksara_tampil_from_arlist);
                        tv_aksara_content_before.setText(str_aksara_tampil_from_arlist);
                    } catch (Exception e) {
                        Log.e("getAksara", "Error di JS Bro!");
                    }


                    /*list_words_translated.add(translated);
                    str_aksara_tampil = TextUtils.join(" ", list_words_translated);
                    tv_aksara_content.setText(str_aksara_tampil);
                    tv_aksara_content_before.setText(str_aksara_tampil);
*/

                    //tv_aksara_content.setText(val);
                    //tv_aksara_content.setText("k(x");
                    // tv_aksara_content_before.setText(val);
                    // Log.d("Before", str_aksara_tampil);
                }
            });
        }
    }

    //==========new script=================

    public void hapus() {
        this.hurufLatin = "";
        this.aksaraBali = "";
        this.cjh = new CekJenisHuruf();
        this.tempHasilKonversi = null;
        this.tempHasilKonversi = new String[100];
        this.tempKalimat = new char[100];
        this.gantungan = false;
        this.cecek = false;
        this.kndsNG = false;
        this.tempKata = new String[100];
        this.indexHrf = 0;
        this.indexKata = 0;
        this.indexHrfKonversi = 0;
        this.jmlGantungan = 0;
    }

    private static byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {
            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bytesArray;
    }

    public void sendBitmap(String filePath, Bitmap bm) {

        try {

            AndroidBmpUtil bmpUtil = new AndroidBmpUtil();
            boolean isSaveResult = false;

            isSaveResult = bmpUtil.save(bm, filePath);
            tv_aksara_content.setDrawingCacheEnabled(false);

            Toast.makeText(getActivity(), "Menyimpan " + filePath + " : " + String.valueOf(isSaveResult), Toast.LENGTH_SHORT).show();

            String encodedImage = Base64.encodeToString(readBytesFromFile(filePath), Base64.DEFAULT);
            Log.d("DATA SEND", "encodedImage: " + encodedImage);

            pDialog.setMessage("Mengirim Data...");
            pDialog.setCancelable(false);
            pDialog.show();

            //*Json Request*//*
            String url = "http://" + server_address + CONFIG.MAIN_URL + CONFIG.SEND_BMP_URL;
            JSONObject data_send = new JSONObject();
            Log.d("DATA SEND", "URL: " + url);

            try {
                //sLog.d("KIRIM",jsonObject_vote.toString());
                data_send.put("filebmp", encodedImage);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                        url,
                        data_send,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    //pDialog.hide();
                                    Log.d("DATA SEND", response.getString("status"));
                                    //hapus file setelah upload selesai
                                    if (myFile.exists()) {
                                        if (myFile.delete()) {
                                            Log.d("Aksara File", "DELETE AFTER SEND: TRUE");
                                        }
                                    }

                                    pDialog.dismiss();
                                    Toast.makeText(getActivity(), "Pengiriman: " + response.getString("status"), Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    pDialog.dismiss();
                                    Toast.makeText(getActivity(), "Tidak dapat mengirim", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                try {

                                    pDialog.dismiss();
                                    Log.e("ERROR", error.getMessage());
                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                        Toast.makeText(getActivity(), "Jaringan Bermasalah",
                                                Toast.LENGTH_LONG).show();
                                    } else if (error instanceof AuthFailureError) {
                                        Toast.makeText(getActivity(), "Username atau Password salah",
                                                Toast.LENGTH_LONG).show();
                                    } else if (error instanceof ServerError) {
                                        Toast.makeText(getActivity(), "Server Error",
                                                Toast.LENGTH_LONG).show();
                                    } else if (error instanceof NetworkError) {
                                        Toast.makeText(getActivity(), "Network Error",
                                                Toast.LENGTH_LONG).show();
                                    } else if (error instanceof ParseError) {
                                        Toast.makeText(getActivity(), "Parse Error",
                                                Toast.LENGTH_LONG).show();
                                    }
                                } catch (Exception e) {
                                    Log.e("Error Response Handler", e.getMessage());
                                }

                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        //headers.put("Content-Type", "application/json");
                        headers.put("Accept", "application/json");
                        //headers.put("Authorization", "Bearer " + CONSTANTS.ACCESS_TOKEN);
                        return headers;
                    }
                };
                //add request to queue
                requestQueue.add(jsonObjectRequest);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            Toast.makeText(getActivity(), "Tidak Dapat Menyimpan Bitmap", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    public void setHurufLatin(String HrfLatin) {
        this.hurufLatin = HrfLatin;
    }

    public String prosesKonversi() {
        this.aksaraBali = "";
        String hrfakhir = this.hurufLatin.substring(this.hurufLatin.length() - 1, this.hurufLatin.length());
        if (!(hrfakhir.equals(".") || hrfakhir.equals(","))) {
            this.hurufLatin += "";
        }
        String tempLatin = this.hurufLatin;
        this.tempKalimat = tempLatin.toCharArray();
        if (tempLatin.contains(" ")) {
            this.tempKata = tempLatin.split(" ");
        } else {
            this.tempKata = new String[1];
            this.tempKata[0] = tempLatin;
        }
        this.tempHasilKonversi = new String[(tempLatin.length() + 10)];
        this.indexHrf = 0;
        while (this.indexHrf < this.tempKalimat.length) {
            int tmpInd = this.indexHrf;
            if (this.tempKalimat[this.indexHrf] == ' ' || this.indexHrf == 0) {
                this.kndsNG = false;
                char hrfDepan = ' ';

                Log.d("Proses Konversi", "indexHrf==' ':" + this.indexHrf);
                Log.d("Proses Konversi", "tempKalimat==' ':" + String.valueOf(this.tempKalimat));
                if (this.indexHrf != 0) {
                    if (this.tempKalimat[this.indexHrf] == ' ') {
                        hrfDepan = this.tempKalimat[this.indexHrf - 1];
                    }
                }

                if (this.indexKata != this.tempKata.length) {
                    this.kndsNG = this.ok.olahKataNG(this.tempKata[this.indexKata]);
                    cekToDatabase(this.tempKata[this.indexKata], hrfDepan);
                    this.indexKata++;
                }
            }
            if (!this.kataDiDB) {
                prosesHuruf(this.indexHrf);
            }
            this.indexHrf++;
        }
        for (int i = 0; i < this.indexHrfKonversi; i++) {
            this.aksaraBali += this.tempHasilKonversi[i];
        }
        return this.aksaraBali;
    }

    private void cekToDatabase(String kataLatin, char hrfDepan) {
        String tempHrfDB = getAksara(kataLatin, hrfDepan);
        this.kataDiDB = false;
        if (cekKataAtDB(kataLatin)) {
            this.kataDiDB = true;
            char[] tempHSL = tempHrfDB.toCharArray();
            for (char valueOf : tempHSL) {
                this.tempHasilKonversi[this.indexHrfKonversi] = String.valueOf(valueOf);
                this.indexHrfKonversi++;
            }
            this.indexHrf = tempHSL.length - 1;
        }
    }

    private void setNilaiVariabel(String[] tempHasilKonversix, int indexHrfKonversix, int indexHrfx, boolean gantunganx, boolean cecekx) {
        this.tempHasilKonversi = tempHasilKonversix;
        this.indexHrfKonversi = indexHrfKonversix;
        this.indexHrf = indexHrfx;
        this.gantungan = gantunganx;
        this.cecek = cecekx;
    }

    public void prosesHuruf(int indHrf) {
        if (this.tempKalimat[indHrf] == 'a') {
            this.knvA = new KonversiHrf_A(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvA.setCecek(this.cecek);
            this.knvA.konversi_A();
            setNilaiVariabel(this.knvA.getTempHasilKonversi(), this.knvA.getIndexHrfKonversi(), this.knvA.getIndexHuruf(), this.knvA.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'e') {
            this.knvE = new KonversiHrf_E(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvE.setGantungan(this.gantungan);
            this.knvE.setCecek(this.cecek);
            this.knvE.konversi_E();
            setNilaiVariabel(this.knvE.getTempHasilKonversi(), this.knvE.getIndexHrfKonversi(), this.knvE.getIndexHuruf(), this.knvE.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'u') {
            this.knvU = new KonversiHrf_U(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvU.setCecek(this.cecek);
            if (this.gantungan) {
                this.knvU.konversi_UKecil();
            } else {
                this.knvU.konversi_U();
            }
            setNilaiVariabel(this.knvU.getTempHasilKonversi(), this.knvU.getIndexHrfKonversi(), this.knvU.getIndexHuruf(), this.knvU.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'o') {
            this.knvO = new KonversiHrf_O(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvO.setGantungan(this.gantungan);
            this.knvO.setCecek(this.cecek);
            this.knvO.konversi_O();
            setNilaiVariabel(this.knvO.getTempHasilKonversi(), this.knvO.getIndexHrfKonversi(), this.knvO.getIndexHuruf(), this.knvO.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'i') {
            this.knvI = new KonversiHrf_I(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvI.setGantungan(this.gantungan);
            this.knvI.setCecek(this.cecek);
            this.knvI.konversi_I();
            setNilaiVariabel(this.knvI.getTempHasilKonversi(), this.knvI.getIndexHrfKonversi(), this.knvI.getIndexHuruf(), this.knvI.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == '\u00e9') {
            this.knvEe = new KonversiHrf_Ee(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvEe.setCecek(this.cecek);
            this.knvEe.konversi_Ee();
            setNilaiVariabel(this.knvEe.getTempHasilKonversi(), this.knvEe.getIndexHrfKonversi(), this.knvEe.getIndexHuruf(), this.knvEe.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'b' || this.tempKalimat[indHrf] == 'B') {
            this.knvB = new KonversiHrf_B(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvB.setGantungan(this.gantungan);
            this.knvB.setCecek(this.cecek);
            this.knvB.konversi_B();
            setNilaiVariabel(this.knvB.getTempHasilKonversi(), this.knvB.getIndexHrfKonversi(), this.knvB.getIndexHuruf(), this.knvB.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'c' || this.tempKalimat[indHrf] == 'C') {
            this.knvC = new KonversiHrf_C(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvC.setGantungan(this.gantungan);
            this.knvC.setCecek(this.cecek);
            this.knvC.konversi_C();
            setNilaiVariabel(this.knvC.getTempHasilKonversi(), this.knvC.getIndexHrfKonversi(), this.knvC.getIndexHuruf(), this.knvC.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'd' || this.tempKalimat[indHrf] == 'D') {
            this.knvD = new KonversiHrf_D(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvD.setGantungan(this.gantungan);
            this.knvD.setCecek(this.cecek);
            this.knvD.konversi_D();
            setNilaiVariabel(this.knvD.getTempHasilKonversi(), this.knvD.getIndexHrfKonversi(), this.knvD.getIndexHuruf(), this.knvD.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'g' || this.tempKalimat[indHrf] == 'G') {
            this.knvG = new KonversiHrf_G(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvG.setGantungan(this.gantungan);
            this.knvG.setCecek(this.cecek);
            this.knvG.konversi_G();
            setNilaiVariabel(this.knvG.getTempHasilKonversi(), this.knvG.getIndexHrfKonversi(), this.knvG.getIndexHuruf(), this.knvG.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'h' || this.tempKalimat[indHrf] == 'H') {
            this.knvH = new KonversiHrf_H(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvH.setGantungan(this.gantungan);
            this.knvH.setCecek(this.cecek);
            this.knvH.konversi_H();
            setNilaiVariabel(this.knvH.getTempHasilKonversi(), this.knvH.getIndexHrfKonversi(), this.knvH.getIndexHuruf(), this.knvH.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'j' || this.tempKalimat[indHrf] == 'J') {
            this.knvJ = new KonversiHrf_J(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvJ.setGantungan(this.gantungan);
            this.knvJ.setCecek(this.cecek);
            this.knvJ.konversi_J();
            setNilaiVariabel(this.knvJ.getTempHasilKonversi(), this.knvJ.getIndexHrfKonversi(), this.knvJ.getIndexHuruf(), this.knvJ.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'k' || this.tempKalimat[indHrf] == 'K') {
            this.knvK = new KonversiHrf_K(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvK.setGantungan(this.gantungan);
            this.knvK.setCecek(this.cecek);
            this.knvK.konversi_K();
            setNilaiVariabel(this.knvK.getTempHasilKonversi(), this.knvK.getIndexHrfKonversi(), this.knvK.getIndexHuruf(), this.knvK.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'l' || this.tempKalimat[indHrf] == 'L') {
            this.knvL = new KonversiHrf_L(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvL.setGantungan(this.gantungan);
            this.knvL.setCecek(this.cecek);
            this.knvL.konversi_L();
            setNilaiVariabel(this.knvL.getTempHasilKonversi(), this.knvL.getIndexHrfKonversi(), this.knvL.getIndexHuruf(), this.knvL.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'm' || this.tempKalimat[indHrf] == 'M') {
            this.knvM = new KonversiHrf_M(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvM.setGantungan(this.gantungan);
            this.knvM.setCecek(this.cecek);
            this.knvM.konversi_M();
            setNilaiVariabel(this.knvM.getTempHasilKonversi(), this.knvM.getIndexHrfKonversi(), this.knvM.getIndexHuruf(), this.knvM.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'n' || this.tempKalimat[indHrf] == 'N') {
            this.knvN = new KonversiHrf_N(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvN.setGantungan(this.gantungan);
            this.knvN.setCecek(this.cecek);
            this.knvN.setKndsNg(this.kndsNG);
            this.knvN.konversi_N();
            setNilaiVariabel(this.knvN.getTempHasilKonversi(), this.knvN.getIndexHrfKonversi(), this.knvN.getIndexHuruf(), this.knvN.kndsGantungan(), this.knvN.kndsCecek());
        } else if (this.tempKalimat[indHrf] == 'p' || this.tempKalimat[indHrf] == 'P') {
            this.knvP = new KonversiHrf_P(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvP.setGantungan(this.gantungan);
            this.knvP.setCecek(this.cecek);
            this.knvP.konversi_P();
            setNilaiVariabel(this.knvP.getTempHasilKonversi(), this.knvP.getIndexHrfKonversi(), this.knvP.getIndexHuruf(), this.knvP.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'r' || this.tempKalimat[indHrf] == 'R') {
            this.knvR = new KonversiHrf_R(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvR.setGantungan(this.gantungan);
            this.knvR.setCecek(this.cecek);
            this.knvR.konversi_R();
            setNilaiVariabel(this.knvR.getTempHasilKonversi(), this.knvR.getIndexHrfKonversi(), this.knvR.getIndexHuruf(), this.knvR.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 's' || this.tempKalimat[indHrf] == 'S') {
            this.knvS = new KonversiHrf_S(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvS.setCecek(this.cecek);
            this.knvS.setGantungan(this.gantungan);
            this.knvS.konversi_S();
            setNilaiVariabel(this.knvS.getTempHasilKonversi(), this.knvS.getIndexHrfKonversi(), this.knvS.getIndexHuruf(), this.knvS.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 't' || this.tempKalimat[indHrf] == 'T') {
            this.knvT = new KonversiHrf_T(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvT.setCecek(this.cecek);
            this.knvT.setKndsGantungan(this.gantungan);
            this.knvT.konversi_T();
            setNilaiVariabel(this.knvT.getTempHasilKonversi(), this.knvT.getIndexHrfKonversi(), this.knvT.getIndexHuruf(), this.knvT.kndsGantungan(), false);
        } else if (this.tempKalimat[indHrf] == 'w' || this.tempKalimat[indHrf] == 'W') {
            this.knvW = new KonversiHrf_W(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvW.setCecek(this.cecek);
            this.knvW.setKndsGantungan(this.gantungan);
            this.knvW.konversi_W();
            this.gantungan = this.knvW.kndsGantungan();
            this.tempHasilKonversi = this.knvW.getTempHasilKonversi();
            this.indexHrfKonversi = this.knvW.getIndexHrfKonversi();
            this.indexHrf = this.knvW.getIndexHuruf();
            this.cecek = false;
        } else if (this.tempKalimat[indHrf] == 'y' || this.tempKalimat[indHrf] == 'Y') {
            this.knvY = new KonversiHrf_Y(this.indexHrfKonversi, indHrf, this.tempKalimat, this.tempHasilKonversi);
            this.knvY.setCecek(this.cecek);
            this.knvY.setGantungan(this.gantungan);
            this.knvY.konversi_Y();
            this.gantungan = this.knvY.kndsGantungan();
            this.tempHasilKonversi = this.knvY.getTempHasilKonversi();
            this.indexHrfKonversi = this.knvY.getIndexHrfKonversi();
            this.indexHrf = this.knvY.getIndexHuruf();
            this.cecek = false;
        } else if (this.tempKalimat[indHrf] == ',' || this.tempKalimat[indHrf] == '.') {
            if (!(!this.cjh.cekJenisHurufKonsonan(this.tempKalimat[indHrf - 1]) || this.cecek || this.tempKalimat[indHrf - 1] == 'h' || this.tempKalimat[indHrf - 1] == 'r')) {
                this.tempHasilKonversi[this.indexHrfKonversi] = "/";
                this.indexHrfKonversi++;
            }
            this.cecek = true;
            this.tempHasilKonversi[this.indexHrfKonversi] = String.valueOf(this.tempKalimat[indHrf]);
            this.indexHrfKonversi++;
        } else if (this.tempKalimat[indHrf] == '1' || this.tempKalimat[indHrf] == '2' || this.tempKalimat[indHrf] == '3' || this.tempKalimat[indHrf] == '4' || this.tempKalimat[indHrf] == '4' || this.tempKalimat[indHrf] == '5' || this.tempKalimat[indHrf] == '6' || this.tempKalimat[indHrf] == '6' || this.tempKalimat[indHrf] == '7' || this.tempKalimat[indHrf] == '8' || this.tempKalimat[indHrf] == '9' || this.tempKalimat[indHrf] == '0') {
            this.tempHasilKonversi[this.indexHrfKonversi] = String.valueOf(this.tempKalimat[indHrf]);
            this.indexHrfKonversi++;
        }
        if (indHrf == this.tempKalimat.length - 1) {
            if (this.tempKalimat[indHrf] == 'h') {
                //ganti
                //this.tempHasilKonversi[indHrf - 1] = ";,";
                this.tempHasilKonversi[indHrf] = ";,";
            } else if (this.tempKalimat[indHrf] != 'r') {
                if (this.tempKalimat[indHrf] == '.') {
                    if (this.cjh.cekJenisHurufKonsonan(this.tempKalimat[this.indexHrf - 1])) {
                    }
                } else if (this.tempKalimat[indHrf] == ',') {
                    if (!this.cjh.cekJenisHurufKonsonan(this.tempKalimat[this.indexHrf - 1]) || this.cecek) {
                        this.tempHasilKonversi[this.indexHrfKonversi] = ",";
                        this.indexHrfKonversi++;
                    } else {
                        this.tempHasilKonversi[this.indexHrfKonversi] = "/,";
                        this.indexHrfKonversi++;
                    }
                } else if (this.cjh.cekJenisHurufKonsonan(this.tempKalimat[this.indexHrf]) && !this.cecek) {
                    this.tempHasilKonversi[this.indexHrfKonversi] = "/,";
                    this.indexHrfKonversi++;
                } else if (this.cjh.cekJenisHurufVokal(this.tempKalimat[this.indexHrf])) {
                    this.tempHasilKonversi[this.indexHrfKonversi] = ",";
                    this.indexHrfKonversi++;
                }
            }
        }
        if (this.gantungan) {
            this.jmlGantungan++;
        }
    }

    public String getAksara(String kataLatin, char hrfDepan) {
        String aksara = "";
        try {
            Cursor d = this.helper.getAllAksara(kataLatin.replace(".", ""));
            if (d != null && d.moveToFirst()) {
                String aks_bali = d.getString(0);
                String aks_gntng = d.getString(1);
                aksara = this.cjh.cekJenisHurufKonsonan(hrfDepan) ? aks_gntng == aks_bali ? "/" + aks_gntng : aks_gntng : aks_bali;
            }
            return aksara;
        } catch (Exception DBException) {
            /*AlertDialog alertDialog = new Builder(this).create();
            alertDialog.setTitle("indeks");
            alertDialog.setMessage(String.valueOf(DBException.toString()));
            alertDialog.setButton("OK", new C00031());
            alertDialog.setIcon(C0000R.drawable.icon);
            alertDialog.show();*/
            Log.e("getAksara", "Error Get Aksara");

            return "error";
        }
    }

    public boolean cekKataAtDB(String kataLatin) {
        boolean adaAksara = false;
        try {
            Cursor c = this.helper.getAdaAksara(kataLatin.replace(".", ""));
            if (c != null && c.moveToFirst()) {
                adaAksara = true;
            }
            return adaAksara;
        } catch (Exception DBException) {
 /*           AlertDialog alertDialog = new Builder(this).create();
            alertDialog.setTitle("indeks");
            alertDialog.setMessage(String.valueOf(DBException.toString()));
            alertDialog.setButton("OK", new C00042());
            alertDialog.setIcon(C0000R.drawable.icon);
            alertDialog.show();*/
            Log.e("cekKataAtDB", "Error Cek Kata At DB");
            return false;
        }
    }

}